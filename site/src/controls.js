/*
controls.js
*/

"use strict";

// Controls Script Container
var controlsScript = ( function () {

	var m_$lastView, m_lastCanvas, m_imageTypes;

	m_imageTypes = [
		"image/bmp", "image/gif", "image/vnd.microsoft.icon", "image/jpeg",
		"image/png", "image/svg+xml", "image/tiff", "image/webp"
	];

	// Initialize buttons
	function initialize() {
		var dragEvents, i;

		// Prevent default behavior on drag events
		function noDrop( e ) {
			e.preventDefault();
			e.stopPropagation();
		}
		dragEvents = [
			"drag", "dragstart", "dragend", "dragover",
			"dragenter", "dragleave", "drop"
		];
		for( i = 0; i < dragEvents.length; i++ ) {
			document.body.addEventListener( dragEvents[ i ], noDrop );
		}

		// Change background on drag over
		function dragOver( e ) {
			document.getElementById( "dragOverPopup" ).style.display = "block";
		}
		dragEvents = [ "dragover", "dragenter" ];
		for( i = 0; i < dragEvents.length; i++ ) {
			document.body.addEventListener( dragEvents[ i ], dragOver );
		}

		// Change background on drag out
		function dragOut( e ) {
			document.getElementById( "dragOverPopup" ).style.display = "none";
		}
		dragEvents = [ "dragleave", "dragend", "drop" ];
		for( i = 0; i < dragEvents.length; i++ ) {
			document.getElementById( "dragOverPopup" )
				.addEventListener( dragEvents[ i ], dragOut );
		}

		// Dropped File
		function droppedFile( e ) {
			var droppedFiles;

			animatorScript.closeAnimator();
			loadImageButtonClicked();
			droppedFiles = e.dataTransfer.files[ 0 ];
			document.getElementById( "loadImageFile" )
				.files = e.dataTransfer.files;
			loadImageFileChanged();
		}
		document.getElementById( "dragOverPopup" )
			.addEventListener( "drop", droppedFile );

		document.getElementById( "newImageButton" ).addEventListener(
			"click", newPictureButtonClicked
		);
		document.getElementById( "loadImageButton" ).addEventListener(
			"click", loadImageButtonClicked
		);
		document.getElementById( "copyImageButton" ).addEventListener(
			"click", copyPictureButtonClicked
		);
		document.getElementById( "saveImageButton" ).addEventListener(
			"click", saveImageButtonPopupClicked
		);
		document.getElementById( "deleteImageButton" ).addEventListener(
			"click", deletePictureButtonClicked
		);
		document.getElementById( "newImageOk" ).addEventListener(
			"click", newImageOkClicked
		);
		document.getElementById( "newImageCancel" ).addEventListener(
			"click", newImageCancelClicked
		);
		document.getElementById( "loadImageSpritesheet" ).addEventListener(
			"change", loadImageSpritesheetChanged
		);
		document.getElementById( "loadImageFile" ).addEventListener(
			"change", loadImageFileChanged
		);
		document.getElementById( "loadImageOk" ).addEventListener(
			"click", loadImageOkClicked
		);
		document.getElementById( "loadImageCancel" ).addEventListener(
			"click", loadImageCancelClicked
		);
		document.getElementById( "saveImageType" ).addEventListener(
			"change", saveImageTypeChanged
		);
		document.getElementById( "saveImageOk" ).addEventListener(
			"click", saveImageOkClicked
		);
		document.getElementById( "saveImageCancel" ).addEventListener(
			"click", saveImageCancelClicked
		);
		document.getElementById( "loadImageSpritesheet" ).addEventListener(
			"change", loadImageDataChanged
		);
		document.getElementById( "loadImageWidth" ).addEventListener(
			"change", loadImageDataChanged
		);
		document.getElementById( "loadImageHeight" ).addEventListener(
			"change", loadImageDataChanged
		);
		document.getElementById( "loadImageMargin" ).addEventListener(
			"change", loadImageDataChanged
		);

		window.addEventListener( "beforeunload", autoSaveWorkspace );
		setTimeout( function () {
			loadAutoSavedWorkspace();
			startAutoSave();
		}, 100 );
	}

	function startAutoSave() {
		var isIdle;

		function setIdle() {
			isIdle = false;
		}

		window.addEventListener( "touchstart", setIdle );
		window.addEventListener( "touchmove", setIdle );
		window.addEventListener( "touchend", setIdle );
		window.addEventListener( "mousemove", setIdle );
		window.addEventListener( "mousedown", setIdle );
		window.addEventListener( "mouseup", setIdle );
		window.addEventListener( "keydown", setIdle );
		window.addEventListener( "keyup", setIdle );

		setInterval( function () {
			if( isIdle ) {
				autoSaveWorkspace();
			}
			isIdle = true;
		}, 5000 );
	}

	function loadAutoSavedWorkspace() {
		var data, i;
		data = JSON.parse( localStorage.getItem( "workspace" ) );
		if( data === null ) {
			return;
		}
		for( i in data.options ) {
			if( i !== "effectsOption" ) {
				optionsTool.updateOption( i, data.options[ i ] );
			}
		}
		colorScript.setColors( data.colors );
		pixel.updateStatusBar();
		pictureScript.deletePicture( "all" );
		for( i = 0; i < data.pictures.length; i++ ) {
			createPixelPicture( data.pictures[ i ] );
		}
	}

	function autoSaveWorkspace() {
		var data;

		document.getElementById( "statusMsg" ).innerHTML = "Autosave";
		data = {
			"pictures": getWorkspaceData(),
			"options": optionsTool.getSettings(),
			"colors": colorScript.getColors()
		};
		localStorage.setItem( "workspace", JSON.stringify( data ) );
		setTimeout( function () {
			if(
				document.getElementById( "statusMsg" ).innerHTML === "Autosave"
			) {
				document.getElementById( "statusMsg" ).innerHTML = "";
			}
		}, 1000 );
	}

	// New Picture Button
	function newPictureButtonClicked() {

		// Clear the action keys
		pixel.clearActionKeys();

		// Show the newImagePopup
		document.getElementById( "newImagePopup" ).style.display = "block";
		document.getElementById( "modalPopup" ).style.display = "block";

		// Focus on the Ok button
		document.getElementById( "newImageOk" ).focus();

	}

	function loadImageButtonClicked() {

		// Clear the action keys
		pixel.clearActionKeys();

		// Show the newImagePopup
		document.getElementById( "loadImagePopup" ).style.display = "block";
		document.getElementById( "modalPopup" ).style.display = "block";

		// Focus on the Ok button
		document.getElementById( "loadImageOk" ).focus();
	}

	function loadImageSpritesheetChanged() {
		
		if( document.getElementById( "loadImageSpritesheet" ).checked ) {
			showHideSpriteSheetForm( false );
		} else {
			showHideSpriteSheetForm( true );
		}
	}

	function showHideSpriteSheetForm( isHidden ) {
		if( isHidden ) {
			document.getElementById( "loadImageWidth" )
				.parentElement.parentElement.style.display = "none";
			document.getElementById( "loadImageHeight" )
				.parentElement.parentElement.style.display = "none";
			document.getElementById( "loadImageMargin" )
				.parentElement.parentElement.style.display = "none";
		} else {
			document.getElementById( "loadImageWidth" )
				.parentElement.parentElement.style.display = "block";
			document.getElementById( "loadImageHeight" )
				.parentElement.parentElement.style.display = "block";
			document.getElementById( "loadImageMargin" )
				.parentElement.parentElement.style.display = "block";
		}
	}

	function loadImageFileChanged( e ) {
		var fileInput, loadImageViewerDiv, loadImageSpriteForm, statsDiv,
			spritesheetCheck;

		fileInput = document.getElementById( "loadImageFile" );
		loadImageViewerDiv = document.getElementById( "loadImageViewer" );
		loadImageSpriteForm = document.getElementById( "loadImageSpriteForm" );
		spritesheetCheck = document.getElementById( "loadImageSpritesheet" );
		statsDiv = document.getElementById( "loadImageStats" );

		if( fileInput.value === "" ) {
			document.getElementById( "loadImageOk" ).disabled = "disabled";
			statsDiv.style.display = "none";
			spritesheetCheck.parentElement.style.display = "none";
		} else {
			// Clear the image viewer
			if( m_$lastView ) {
				m_$lastView.removeScreen();
				m_$lastView = null;
			}
			loadImageViewerDiv.innerHTML = "";
			loadImageViewerDiv.style.backgroundImage = "";

			// Load the image viewer based on data type
			if( m_imageTypes.indexOf( fileInput.files[ 0 ].type ) > - 1) {
				loadImageSpriteForm.style.display = "inline-block";
				spritesheetCheck.parentElement.style.display = "inline-block";
				loadImageSpritesheetChanged();
				loadImageViewer( fileInput.files[ 0 ] );
				statsDiv.style.display = "none";
			} else {
				loadImageSpriteForm.style.display = "none";
				statsDiv.style.display = "inline-block";
				showHideSpriteSheetForm( true );
				loadPixelViewer( fileInput.files[ 0 ] );
			}

			// Remove the diasabled attribute from the ok button
			document.getElementById( "loadImageOk" )
				.removeAttribute( "disabled" );
		}
	}

	function loadPixelViewer( file ) {
		loadPixelFile( file, function ( data ) {
			var loadImageViewer, i;

			// Get load image viewer
			loadImageViewer = document.getElementById( "loadImageViewer" );

			if( ! validatePixelFormat( data ) ) {
				loadImageViewer.style.backgroundImage = "none";
				document.getElementById( "loadImagePictures" ).innerHTML = "";
				document.getElementById( "loadImageType" )
					.innerHTML = "Invalid File";
				document.getElementById( "loadImagePicturesTitle" )
					.innerHTML = "";
				document.getElementById( "loadImageOk" ).disabled = "disabled";
				return;
			}
			if( data.type === "workspace" ) {
				loadImageViewer.style.backgroundImage = "none";
				document.getElementById( "loadImageType" )
					.innerHTML = "Workspace";
				document.getElementById( "loadImagePictures" )
					.innerHTML = data.pictures.length;
				document.getElementById( "loadImagePicturesTitle" )
					.innerHTML = "Pictures:";
			} else if( data.type === "layers" ) {
				document.getElementById( "loadImageType" )
					.innerHTML = "Image";
				document.getElementById( "loadImagePictures" )
					.innerHTML = data.pictures[ 0 ].length;
				document.getElementById( "loadImagePicturesTitle" )
					.innerHTML = "Layers:";
				for( i = 0; i < data.pictures[ 0 ].length; i++ ) {
					drawViewerLayer( data.pictures[ 0 ][ i ] );
				}
			}
		} );
	}

	function validatePixelFormat( data ) {
		var i, j, layer;

		if(
			! data.type ||
			! qbs.util.isArray( data.pictures ) ||
			data.pictures.length === 0
		) {
			return false;
		}

		for( i = 0; i < data.pictures.length; i++ ) {
			if( data.pictures[ i ].length === 0 ) {
				return false;
			}
			for( j = 0; j < data.pictures[ i ].length; j++ ) {
				layer = data.pictures[ i ][ j ];
				if(
					! qbs.util.isInteger( layer.alpha ) ||
					layer.hidden === undefined ||
					typeof layer.img !== "string" ||
					layer.img.indexOf( "data:image/png;base64" ) !== 0
				) {
					return false;
				}
			}
		}
		return true;
	}

	function drawViewerLayer( layer ) {
		loadImage( layer.img, function ( canvas ) {
			if( m_$lastView == null ) {
				m_$lastView = $.screen(
					canvas.width + "x" + canvas.height,
					document.getElementById( "loadImageViewer" )
				);
			}
			m_$lastView.drawImage( canvas, 0, 0, 0, 0, 0, layer.alpha );
		}, true );
	}

	function loadPixelFile( file, callback ) {
		var blob;

		blob = new Blob( [ file ], { "type": "application/json" } );
		blob.text().then( function ( text ) {
			var data;

			try {
				data = JSON.parse( text );
			} catch( ex ) {
				data = {};
			}
			callback( data );
		} );
	}

	function loadImageViewer( file ) {
		loadImage( file, function ( canvas ) {
			var $view;

			$view = $.screen(
				canvas.width + "x" + canvas.height,
				document.getElementById( "loadImageViewer" )
			);
			$view.drawImage( canvas, 0, 0 );
			m_$lastView = $view;
			m_lastCanvas = canvas;
			drawSpriteSheetGrid();
		} );
	}

	function loadImageDataChanged() {
		drawSpriteSheetGrid();
	}

	function drawSpriteSheetGrid() {
		var data, width, height, x, y, colors, i, y2, $temp;

		data = getLoadImageData();
		if( ! m_$lastView ) {
			return;
		}
		m_$lastView.cls();
		m_$lastView.drawImage( m_lastCanvas );
		if( data.spritesheet ) {
			width = m_$lastView.canvas().width;
			height = m_$lastView.canvas().height;
			$temp = $.screen( width + "x" + height, null, true );

			// Draw outer white semi-transparent rectangle
			colors = [ pixel.color1, pixel.color2 ];
			$temp.setPen( "pixel", 1 );
			$temp.setColor( "#00000056" );
			y2 = 0;
			for(
				y = data.margin; y < height; y += data.height + data.margin
			) {
				i = y2 % 2;
				y2 += 1;
				for(
					x = data.margin; x < width; x += data.width + data.margin
				) {
					$temp.setColor( colors[ i % 2 ] );
					$temp.rect(
						x, y, data.width, data.height, colors[ i % 2 ]
					);
					i += 1;
				}
			}

			$temp.render();
			m_$lastView.drawImage( $temp, 0, 0 );
			$temp.removeScreen();
		}
	}

	function getLoadImageData() {
		var data;

		data = {
			"file": document.getElementById( "loadImageFile" ).files[ 0 ],
			"spritesheet": document.getElementById( "loadImageSpritesheet" )
				.checked,
			"width": parseInt(
					document.getElementById( "loadImageWidth" ).value
				),
			"height": parseInt(
					document.getElementById( "loadImageHeight" ).value
				),
			"margin": parseInt(
					document.getElementById( "loadImageMargin" ).value
				)
		};

		// Validate input
		if( isNaN( data.width ) || data.width < 1 ) {
			data.width = 1;
		}
		if( isNaN( data.height ) || data.height < 1 ) {
			data.height = 1;
		}
		if( isNaN( data.margin ) || data.margin < 0 ) {
			data.margin = 0;
		}

		return data;
	}

	// Ok Button on New Image
	function newImageOkClicked() {
		var width, height, count, i;

		width = parseInt( document.getElementById( "newImageWidth" ).value );
		height = parseInt( document.getElementById( "newImageHeight" ).value );
		count = parseInt( document.getElementById( "newImageCount" ).value );

		// Validate input
		if( isNaN( width ) || width < 1 ) {
			width = 1;
		}
		if( isNaN( height ) || height < 1 ) {
			height = 1;
		}
		if( isNaN( count ) || count < 1 ) {
			count = 1;
		}

		if( document.getElementById( "newImageWorkspace" ).checked ) {
			pictureScript.deletePicture( "all" );
		}

		for( i = 0; i < count; i++ ) {
			pictureScript.createNewPicture( width, height );
		}

		hideNewImagePopup( document.getElementById( "newImagePopup" ) );
	}

	// New Image Cancelled
	function newImageCancelClicked() {
		hideNewImagePopup( document.getElementById( "newImagePopup" ) );
	}

	// Ok Button on Load Image
	function loadImageOkClicked() {
		var inputData;

		inputData = getLoadImageData();

		if( m_imageTypes.indexOf( inputData.file.type ) > - 1) {
			if( inputData.spritesheet ) {
				importSpriteSheet(
					inputData.file, inputData.width, inputData.height, inputData.margin
				);
			} else {
				importImage( inputData.file );
			}
		} else {
			loadPixelFile( inputData.file, function ( data ) {
				var i;
				if( ! validatePixelFormat( data ) ) {
					alert( "Invalid data format" );
					return;
				}
				if( data.type === "workspace" ) {
					pictureScript.deletePicture( "all" );
				}
				for( i = 0; i < data.pictures.length; i++ ) {
					createPixelPicture( data.pictures[ i ] );
				}
			} );
		}

		hideNewImagePopup( document.getElementById( "loadImagePopup" ) );
	}

	function createPixelPicture( layers ) {
		var i;

		for( i = 0; i < layers.length; i++ ) {
			createPictureLayer( layers[ i ], i === 0, i === layers.length - 1 );
		}

	}

	function createPictureLayer( layer, isFirst, isLast ) {
		loadImage( layer.img, function ( canvas ) {
			if( isFirst ) {
				pictureScript.createNewPicture( canvas.width, canvas.height );
				layerScript.changeLayerAlpha(
					pixel.activeLayer.id, layer.alpha
				);
				layerScript.setLayerVisibility(
					pixel.activeLayer.id, layer.hidden
				);
				pixel.activeLayer.$screen.canvas().parentElement.parentElement
					.querySelector( "input[type=checkbox]" )
					.checked = ! layer.hidden;
				pixel.activeLayer.$screen.canvas().parentElement.parentElement
					.querySelector( "input[type=range]" )
					.value = layer.alpha;
			} else {
				layerScript.createNewLayer(
					pixel.activePicture, null, layer.alpha, layer.hidden
				);
			}
			pixel.activeLayer.$screen.drawImage( canvas, 0, 0 );
			layerScript.refreshTemp();
			layerScript.drawLayers();
			if( isLast ) {
				// Click on the first picture
				document.querySelectorAll( ".previewItem" )[ 0 ].click();
				document.querySelectorAll( ".layer-item" )[ 0 ].click();
			}
		}, true );
	}

	// Load Image Cancelled
	function loadImageCancelClicked() {
		hideNewImagePopup( document.getElementById( "loadImagePopup" ) );
	}

	function importSpriteSheet( file, width, height, margin ) {
		loadImage( file, function ( canvas ) {
			var x, y;
			for( y = margin; y < canvas.width; y += height + margin ) {
				for( x = margin; x < canvas.width; x += width + margin ) {
					pictureScript.createNewPicture( width, height );
					pixel.activeLayer.$screen.canvas().getContext( "2d" )
						.drawImage(
							canvas,
							x, y, width, height,
							0, 0, width, height
						);
					layerScript.refreshTemp();
					layerScript.drawLayers();
				}
			}
		} );
	}

	function importImage( file ) {
		loadImage( file, function ( canvas ) {
			pictureScript.createNewPicture( canvas.width, canvas.height );
			pixel.activeLayer.$screen.drawImage( canvas, 0, 0 );
			layerScript.refreshTemp();
			layerScript.drawLayers();
		} );
	}

	function loadImage( file, callback, isSrc ) {
		var img, canvas, context;

		img = new Image();
		canvas = document.createElement( "canvas" );
		context = canvas.getContext( "2d" );
		if( isSrc ) {
			img.src = file;
		} else {
			img.src = URL.createObjectURL( file );
		}
		img.onload = function() {
			canvas.width = img.width;
			canvas.height = img.height;
			context.drawImage( img, 0, 0 );
			callback( canvas );
			if( ! isSrc ) {
				URL.revokeObjectURL( img.src );
			}
		};
	}

	function saveImageTypeChanged() {
		var saveImageType = document.getElementById( "saveImageType" );

		if( saveImageType.value === "spritesheet" ) {
			document.getElementById( "saveSpriteMargin" )
				.parentElement.parentElement.style.display = "block";
			document.getElementById( "addFlipX" )
				.parentElement.parentElement.style.display = "block";
			document.getElementById( "addFlipY" )
				.parentElement.parentElement.style.display = "block";
		} else {
			document.getElementById( "saveSpriteMargin" )
				.parentElement.parentElement.style.display = "none";
			document.getElementById( "addFlipX" )
				.parentElement.parentElement.style.display = "none";
			document.getElementById( "addFlipY" )
				.parentElement.parentElement.style.display = "none";
		}
	}

	function saveImageOkClicked() {
		var filename, spriteMargin, addFlipX, addFlipY;

		filename = document.getElementById( "saveFilename" ).value;
		if( filename.length === 0 ) {
			filename = "image.png";
		}

		switch( document.getElementById( "saveImageType" ).value ) {
			case "workspace":
				saveWorkspace( filename );
				break;
			case "spritesheet":
				spriteMargin = parseInt(
					document.getElementById( "saveSpriteMargin" ).value
				);
				if( isNaN( spriteMargin ) || spriteMargin < 1 ) {
					spriteMargin = 1;
				}
				addFlipX = document.getElementById( "addFlipX" ).checked;
				addFlipY = document.getElementById( "addFlipY" ).checked;
				saveSpriteSheet( spriteMargin, filename, addFlipX, addFlipY );
				break;
			case "layers":
				saveLayers( filename );
				break;
			case "image":
				saveImage( pixel.activePicture.$preview.canvas(), filename );
				break;
		}

		hideNewImagePopup( document.getElementById( "saveImagePopup" ) );
	}

	function saveWorkspace( filename ) {
		var data, blob;

		data = {
			"name": filename,
			"type": "workspace",
			"pictures": getWorkspaceData()
		};

		blob = new Blob(
			[ JSON.stringify( data ) ],
			{ "type": "application/json" }
		);

		saveData( blob, filename + ".workspace.pixel" );
	}

	function getWorkspaceData() {
		var pictures, i;

		pictures = [];
		for( i = 0; i < pixel.pictures.length; i++ ) {
			pictures.push( getPictureData( pixel.pictures[ i ] ) );
		}
		return pictures;
	}

	function saveLayers( filename ) {
		var data, blob;

		data = {
			"name": filename,
			"type": "layers",
			"pictures": []
		};

		data.pictures.push( getPictureData( pixel.activePicture ) );

		blob = new Blob(
			[ JSON.stringify( data ) ],
			{ "type": "application/json" }
		);

		saveData( blob, filename + ".image.pixel" );
	}

	function getPictureData( picture ) {
		var layers, i, layerData;

		layers = [];
		for( i = 0; i < picture.orderedLayers.length; i++ ) {
			layerData = {
				"alpha": parseInt( picture.orderedLayers[ i ].alpha ),
				"hidden": !!( picture.orderedLayers[ i ].hidden ),
				"img": picture.orderedLayers[ i ].$screen.canvas().toDataURL()
			};
			layers.push( layerData );
		}

		return layers;
	}

	function saveSpriteSheet( margin, filename, addFlipX, addFlipY ) {
		var width, height, i, frames, canvas, context, x, y, picture,
			tempCanvas, tempContext, action;

		// Get the dimensions from the largest picture
		width = 0;
		height = 0;
		for( i = 0; i < pixel.pictures.length; i++ ) {
			if( pixel.pictures[ i ].width > width ) {
				width = pixel.pictures[ i ].width;
			}
			if( pixel.pictures[ i ].height > height ) {
				height = pixel.pictures[ i ].height;
			}
		}

		frames = pixel.pictures.length;

		// Add additonal frames
		if( addFlipX ) {
			frames += pixel.pictures.length;
		}
		if( addFlipY ) {
			frames += pixel.pictures.length;
		}

		// Create the canvas
		canvas = document.createElement( "canvas" );
		canvas.width = Math.ceil( Math.sqrt( frames ) ) * ( width + margin ) +
			margin;
		canvas.height = Math.ceil( Math.sqrt( frames ) ) * ( height + margin ) +
			margin;
		context = canvas.getContext( "2d" );

		if( addFlipX || addFlipY ) {
			i = 0;
			tempCanvas = document.createElement( "canvas" );
			tempCanvas.width = width;
			tempCanvas.height = height;
			tempContext = tempCanvas.getContext( "2d" );
		}

		// Draw the sprites
		action = "drawImage";
		i = 0;
		for( y = margin; y < canvas.height; y += height + margin ) {
			for( x = margin; x < canvas.width; x += width + margin ) {
				picture = pixel.pictures[ i ];
				if( action === "drawImage" ) {
					context.drawImage( picture.$preview.canvas(), x, y );
				} else if( action === "flipX" || action === "flipY" ) {
					tempContext.clearRect( 0, 0, width, height );
					tempContext.drawImage(
						picture.$preview.canvas(), 0, 0
					);
					effectsScript.effectsFlip(
						action === "flipX", tempCanvas
					);
					context.drawImage( tempCanvas, x, y );
				}
				i += 1;
				if( i >= pixel.pictures.length ) {
					i = 0;
					if( action === "drawImage" ) {
						if( addFlipX ) {
							action = "flipX";
						} else if( addFlipY ) {
							action = "flipY";
						} else {
							action = "none";
						}
					} else if( action === "flipX" ) {
						if( addFlipY ) {
							action = "flipY";
						} else {
							action = "none";
						}
					} else {
						action = "none";
					}
				}
			}
		}

		saveImage( canvas, filename );
	}

	function saveImage( canvas, filename ) {
		canvas.toBlob( function ( blob ) {
			saveData( blob, filename + ".png" );
		}, "image/png", 1.0 );
	}

	function saveData( blob, filename ) {
		var a;

		a = document.createElement( "a" );
		a.href = URL.createObjectURL( blob );
		a.download = filename;
		document.body.appendChild( a );
		a.click();
		a.parentElement.removeChild( a );
		URL.revokeObjectURL( a.href );
	}

	function saveImageCancelClicked() {
		hideNewImagePopup( document.getElementById( "saveImagePopup" ) );
	}

	// Hide image popup
	function hideNewImagePopup( popup ) {
		popup.style.display = "none";
		document.getElementById( "modalPopup" ).style.display = "none";

		// Restore the action keys
		pixel.setActionKeys();
	}

	// Copy picture
	function copyPictureButtonClicked() {
		var original = pixel.activePicture;
		pictureScript.createNewPicture( original.width, original.height );
		//pixel.activePicture.$temp.drawImage( original.$temp );
		layerScript.copyLayers( original );
		layerScript.refreshTemp();
		layerScript.drawLayers();
	}

	function saveImageButtonPopupClicked() {

		// Clear the action keys
		pixel.clearActionKeys();
	
		// Show the newImagePopup
		document.getElementById( "saveImagePopup" ).style.display = "block";
		document.getElementById( "modalPopup" ).style.display = "block";

		// Focus on the Ok button
		document.getElementById( "saveImageOk" ).focus();

	}

	// Delete picturew
	function deletePictureButtonClicked() {
		pictureScript.deletePicture( pixel.activePicture.id );
	}

	// Picture Script return API
	return {
		"initialize": initialize
	};

	// End of file encapsulation
} )();
