
const DOMS = {
	"pic1": "#preview .previewItem.selected-tool canvas",
	"screen": "#screen",
	"black": ".color-row:nth-child(2) .color-button:nth-child(1)",
	"red": ".color-row:nth-child(3) .color-button:nth-child(1)",
	"pink": ".color-row:nth-child(4) .color-button:nth-child(1)",
	"purple": ".color-row:nth-child(5) .color-button:nth-child(1)",
	"light-purple": ".color-row:nth-child(6) .color-button:nth-child(1)",
	"blue": ".color-row:nth-child(7) .color-button:nth-child(1)",
	"cyan": ".color-row:nth-child(8) .color-button:nth-child(1)",
	"green": ".color-row:nth-child(9) .color-button:nth-child(1)",
	"light-green": ".color-row:nth-child(10) .color-button:nth-child(1)",
	"yellow": ".color-row:nth-child(11) .color-button:nth-child(1)",
	"brown": ".color-row:nth-child(12) .color-button:nth-child(1)",
	"light-orange": ".color-row:nth-child(13) .color-button:nth-child(1)",
	"orange": ".color-row:nth-child(14) .color-button:nth-child(1)",
};

const BUTTONS = {
	"0": "none",
	"1": "left",
	"2": "right",
	"3": "middle"
};

async function change( page, selector, value ) {
	await page.evaluate( function ( selector, value ) {
		var element, event;

		// Update value
		element = document.getElementById( selector );
		element.value = value;

		// Dispatch event
		event = new Event( "change" );
		element.dispatchEvent(event);

	}, selector, value );
}

async function getRect( page, selector ) {
	if( DOMS[ selector ] ) {
		selector = DOMS[ selector ];
	}
	return await page.evaluate( function ( selector ) {
		var item = document.querySelector( selector );
		rect = item.getBoundingClientRect();
		return {
			"x": rect.x, "y": rect.y,
			"width": rect.width, "height": rect.height
		};
	}, selector );
}

async function runSteps( page, steps ) {
	var rect, i, id, action, x, y, button, value;

	rect = await getRect( page, "#screen" );

	for( i = 0; i < steps.length; i++ ) {
		id = steps[i ][ 0 ];
		action = steps[ i ][ 1 ];
		x = steps[ i ][ 2 ];
		y = steps[ i ][ 3 ];
		button = BUTTONS[ steps[ i ][ 4 ] ];
		if( button === "none" ) {
			button = null;
		} else {
			button = { "button": button };
		}
		value = steps[ i ][ 5 ];

		x = Math.round( rect.x + ( x * rect.width ) );
		y = Math.round( rect.y + ( y * rect.height ) );

		switch( action ) {
			case "md":
				if( button !== null ) {
					await page.mouse.move( x, y, button );
					await page.mouse.down( button );
				} else {
					await page.mouse.move( x, y );
					await page.mouse.down();
				}
				break;
			case "mm":
				await page.mouse.move( x, y );
				break;
			case "mu":
				if( button !== null ) {
					await page.mouse.up( button );
				} else {
					await page.mouse.up();
				}
				break;
			case "click":
				if( id === "grid" ) {
					console.log( "Grid Click", x, y );
					await page.mouse.move( x, y );
					await page.mouse.down();
					await page.mouse.up();
				} else if( id !== "" ) {
					console.log( "click", id );
					await page.click( "#" + id );
					await page.waitFor( 100 );
				}
				break;
			case "update":
				//console.log( "update", id, value );
				await change( page, id, value );
				break;
		}
		await page.waitFor( 10 );
	}
}

module.exports = {
	"change": change,
	"getRect": getRect,
	"runSteps": runSteps,
	"doms": DOMS
};
