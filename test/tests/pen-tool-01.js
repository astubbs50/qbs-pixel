const HELPER = require( "../test-helper.js" );

function getViewPort() {
	return {
		"width": 1400,
		"height": 800,
		"deviceScaleFactor": 1
	};
}

// Run Test
async function run( page ) {

	let rect = await HELPER.getRect( page, "screen" );
	let x = rect.x;
	let y = rect.y;

	// Draw regular
	x += 30;
	y += 30;
	await draw( 1 );

	// Draw size 2
	await page.click( HELPER.doms[ "red" ] );
	HELPER.change( page, "sizeInput", 2 );
	y += 40;
	await draw( 1 );

	// Draw circle shape
	await page.click( HELPER.doms[ "green" ] );
	await page.click( "#shapeCircle" );
	y += 40;
	await draw( 1 );

	// Draw with noise
	await page.click( HELPER.doms[ "blue" ] );
	await HELPER.change( page, "noiseInput", 200 );
	y += 40;
	await draw( 1 );

	// Draw aliased
	await page.click( HELPER.doms[ "pink" ] );
	await HELPER.change( page, "noiseInput", 0 );
	await page.click( "#drawAliased" );
	y += 40;
	await draw( 1 );

	// Draw mirror X
	await page.click( HELPER.doms[ "purple" ] );
	await page.click( "#drawPixel" );
	await HELPER.change( page, "sizeInput", 1 );
	await page.click( "#reflectX" );
	y += 40;
	await draw( 0.25 );

	// Draw mirror Y
	await page.click( HELPER.doms[ "light-purple" ] );
	await page.click( "#reflectY" );
	y += 40;
	await draw( 0.25 );

	// Draw mirror XY
	await page.click( HELPER.doms[ "orange" ] );
	await page.click( "#reflectXY" );
	y += 40;
	await draw( 0.25 );

	async function draw( pct ) {

		// Draw a pixel
		await page.mouse.move( x, y );
		await page.mouse.down();
		await page.mouse.up();

		// Draw line
		x += 60;
		await page.mouse.move( x, y );
		await page.mouse.down();
		x += 615 * pct;
		await page.mouse.move( x, y, { "steps": 20 } );
		await page.mouse.up();

		x -= 615 * pct + 60;
	}

	return {
		"isFullscreen": false,
		"capture": "pic1"
	};
}

module.exports = {
	"getViewPort": getViewPort,
	"run": run
};