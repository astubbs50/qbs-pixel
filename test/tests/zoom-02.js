const HELPER = require( "../test-helper.js" );

function getViewPort() {
	return {
		"width": 1400,
		"height": 800,
		"deviceScaleFactor": 1
	};
}

// Run Test
async function run( page ) {

	await HELPER.runSteps( page, steps );

	return {
		"isFullscreen": false,
		"capture": "pic1"
	};
}

module.exports = {
	"getViewPort": getViewPort,
	"run": run
};

var steps = [
	[ "reflectXY", "click", 0, 0, 0, "" ],
	[ "grid", "click", 0.092, 0.097, 0, "" ],
	[ "zoom-tool", "click", 0, 0, 0, "" ],
	[ "grid", "click", 0.093, 0.092, 0, "" ],
	[ "grid", "click", 0.093, 0.092, 0, "" ],
	[ "grid", "click", 0.093, 0.092, 0, "" ],
	[ "grid", "click", 0.093, 0.092, 0, "" ],
	[ "grid", "click", 0.093, 0.092, 0, "" ],
	[ "grid", "click", 0.093, 0.092, 0, "" ],
	[ "pen-tool", "click", 0, 0, 0, "" ],
	[ "grid", "click", 0.213, 0.091, 0, "" ],
	[ "grid", "click", 0.213, 0.152, 0, "" ],
	[ "grid", "click", 0.201, 0.224, 0, "" ],
	[ "grid", "click", 0.163, 0.227, 0, "" ],
	[ "grid", "click", 0.095, 0.226, 0, "" ]
];
