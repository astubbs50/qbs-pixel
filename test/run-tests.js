"use strict";

// String Constants
const HOME = "http://localhost:8080/site/";
const TESTS_REF = "./tests/";
const TESTS_FOLDER = "test/tests/";
const SCREENSHOTS_FOLDER = "./test/screenshots/";
const TEST_HTML_ID = "test_";
const TEST_SCREENSHOT_URL = "../test/screenshots/";
const TEMPLATE_FILE = "./test/index-template.html";
const INDEX_FILE = "./test/index.html";
const INDEX_URL = "http://localhost:8080/test/";
const BROWSER1 = "\"C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe\"";
const BROWSER2 = "chromium-browser";
const IMAGE_COMPARE_URL = "http://localhost:8080/test/image-compare.html";

// Libraries
const PUPPETEER = require( "puppeteer" );
const FS = require( "fs" );
const PNG = require( "pngjs" ).PNG;
const CMD = require( "node-cmd" );

// My Libraries
const HELPER = require( "./test-helper.js" );

// Start Testing
( async function() {

	var g_files, g_browser, g_totalTests, g_testCount, g_testResults;

	g_files = FS.readdirSync( TESTS_FOLDER );
	g_browser = await PUPPETEER.launch();

	// Remove none javascript files
	g_files = g_files.filter( function ( val ) {
		return val.lastIndexOf( ".js" ) === val.length - 3;
	} );

	// Run the tests
	g_totalTests = g_files.length;
	g_testCount = 0;

	for( let i = 0; i < g_files.length; i++ ) {
		runTest( g_files[ i ] );
	}

	await waitForTests();
	await g_browser.close();

	// Build test results
	g_testResults = {};

	g_testCount = 0;
	for( let i = 0; i < g_files.length; i++ ) {
		compareImages( g_files[ i ] );
	}

	await waitForTests();

	// Prepare the HTML Results
	let mismatchCount = 0;
	let newTestCount = 0;
	let strStats = "";
	let errors = [];
	let strTests = "";
	for( let i in g_testResults ) {
		let fileName = TEST_SCREENSHOT_URL + i + ".png";
		let fileNameNew = TEST_SCREENSHOT_URL + i + ".new.png";
		let testData = {
			"id": TEST_HTML_ID + i,
			"name": i
		};
		let diff = g_testResults[ i ];
		strTests += "\n\t";
		strTests += "\n\t\t<div id='" + testData.id + "'></div>";
		strTests += "\n\t\t<h2>" + i + "</h2>";
		if( diff === 0 ) {
			strTests += "<span class='good'>MATCHED</span>";
			testData.type = "Good";
		} else if( diff === "Not Verified" ) {
			strTests += "<span class='neutral'>NOT Verified</span>";
			testData.type = "New Image";
			errors.push( testData );
			newTestCount += 1;
		} else {
			strTests += "<span class='error'>NOT MATCHED - Difference: " +
				diff + "</span>";
				testData.type = "Error";
			errors.push( testData );
			mismatchCount += 1;
		}
		strTests += "\n\t\t<a href='#stats'>Go back</a></div>";
		strTests += "\n\t\t<br />";
		let f1 = encodeURIComponent( fileName );
		let f2 = encodeURIComponent( fileNameNew );
		let imageCompareUrl = IMAGE_COMPARE_URL + "?file1=" + f1 +
			"&file2=" + f2;
		strTests += "\n\t\t<a href='" + imageCompareUrl + "' target='_blank'>" +
			"<img src='" + fileName + "' /></a>";
		strTests += "\n\t\t<a href='" + imageCompareUrl + "' target='_blank'>" + 
			"<img src='" + fileNameNew + "' /></a>";
	}

	if( mismatchCount === 0 && newTestCount === 0 ) {
		strStats = "\n\t\t\t<span class='good'>All images match!</span>\n\t\t";
	} else {
		strStats += "\n\t\t\t<span class='error'>Discrepancies Found</span>";
		if( mismatchCount ) {
			strStats += "\n\t\t\t<span class='error'>" + mismatchCount +
				" mismatched images.</span>";
			if( ! newTestCount ) {
				strStats += "\n\t\t";
			}
		}
		if( newTestCount ) {
			strStats += "\n\t\t\t<span class='neutral'>" + newTestCount +
				" new images not verified.</span>\n\t\t";
		}
		if( errors.length > 0 ) {
			strStats += "\n\t\t\t<ol>";
		}
		for( let i = 0; i < errors.length; i++ ) {
			strStats += "\n\t\t\t\t<li><a href='#" + TEST_HTML_ID +
				errors[ i ].id + "'>" +
				errors[ i ].name + " - " + errors[ i ].type +
				"</a></li>";
		}
		if( errors.length > 0 ) {
			strStats += "\n\t\t\t</ol>";
		}
	}

	// Write the final HTML
	let htmlFileName = TEMPLATE_FILE;
	let strHtml = FS.readFileSync( htmlFileName ).toString();
	strHtml = strHtml.replace( "[TEST-STATS]", strStats );
	strHtml = strHtml.replace( "[WEB-TESTS]", strTests );
	FS.writeFileSync( INDEX_FILE, strHtml );

	//Set the command to startup chrome and point to the home page
	let browserUrl = BROWSER1;
	let cmdStr = browserUrl + " " + INDEX_URL;

	//Launch Chrome with link to test file
	CMD.get( cmdStr, function ( err, data, stderr ) {
		if( err ) {
			g_StrLog += "Error: " + err + "\n";
		}
		if( data ) {
			console.log( data );
		}
		if( stderr ) {
			g_StrLog += "Error: " + stderr + "\n";
		}
	} );

	async function runTest( testName ) {

		// Log the test name
		console.log( "Running test:" + testName );

		// Open a new page and navigate to the home page
		let page = await g_browser.newPage();
		await page.goto( HOME );

		// Load the test code
		let testCode = require( TESTS_REF + testName );

		// Wait for 2.5 seconds
		await page.waitFor( 2500 );

		// Set the page view port
		let viewport = testCode.getViewPort();
		await page.setViewport( viewport );

		// Run the test
		let result = await testCode.run( page );

		// Set the fileName
		let fileName = SCREENSHOTS_FOLDER + testName + ".png";
		if( FS.existsSync( fileName ) ) {
			fileName = SCREENSHOTS_FOLDER + testName + ".new.png";
		}

		// Save a screenshot
		if( result.isFullscreen ) {
			await page.screenshot( { "path": fileName } );
		} else {
			let rect = await HELPER.getRect(
				page, HELPER.doms[ result.capture ]
			);
			await page.screenshot( {
				"path": fileName,
				"clip": {
					"x": rect.x, "y": rect.y,
					"width": rect.width, "height": rect.height
				}
			} );
		}
		g_testCount += 1;
	}

	async function waitForTests() {
		let waitResolve;
		async function wait() {
			console.log( g_testCount + " of " + g_totalTests + " complete" );
			if( g_testCount < g_totalTests ) {
				setTimeout( wait, 1000 );
			} else {
				console.log( "Done" );
				waitResolve();
			}
		}
		console.log( "Waiting..." );
		setTimeout( wait, 1000 );
		return new Promise( function( resolve ) { 
			waitResolve = resolve;
		} );
	}

	function compareImages( testName ) {
		let fileName = SCREENSHOTS_FOLDER + testName + ".png";
		let fileNameNew = SCREENSHOTS_FOLDER + testName + ".new.png";
		if( ! FS.existsSync( fileNameNew ) ) {
			console.log( "NOT VERIFIED: " + testName );
			g_testResults[ testName ] = "Not Verified";
			g_testCount += 1;
			return;
		}
		let imagesParsedCount = 0;
		let img1 = FS.createReadStream( fileName ).pipe( new PNG() )
			.on( "parsed", parseImages );
		let img2 = FS.createReadStream( fileNameNew ).pipe( new PNG() )
			.on( "parsed", parseImages );

		function parseImages() {
			imagesParsedCount += 1;
			if( imagesParsedCount < 2 ) {
				return;
			}
			let diff = Math.round( compareImageData( img1, img2 ) * 100 ) / 100;
			if( diff === 0 ) {
				console.log( "SUCCESS: " + testName );
			} else {
				console.log( "FAILED: " + testName + " - " + diff );
			}
			g_testResults[ testName ] = diff;
			g_testCount += 1;
		}
	}

	function compareImageData( img1, img2 ) {

		var x, y, i, diff, pixelD, width, height;
	
		//console.log( img1.data.length, img2.data.length );
		diff = 0;
		width = img1.width;
		height = img1.height;
	
		for ( y = 0; y < height; y++ ) {
			for ( x = 0; x < width; x++ ) {
				i = ( width * y + x ) << 2;
				pixelD = 0;
				pixelD += Math.abs( img1.data[ i ] - img2.data[ i ] );
				pixelD += Math.abs( img1.data[ i + 1 ] - img2.data[ i + 1 ] );
				pixelD += Math.abs( img1.data[ i + 2 ] - img2.data[ i + 2 ] );
				pixelD += Math.abs( img1.data[ i + 3 ] - img2.data[ i + 3 ] );
				diff += ( pixelD / 1020 );
			}
		}
	
		return diff;
	}

} )();
