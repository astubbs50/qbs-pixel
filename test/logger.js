
setTimeout( function () {
	var rect;
	var moves = [];
	var isMoving = false;
	var isDragging = false;
	var screen = document.getElementById( "screen" );
	var dragIndex;
	var last;

	rect = screen.getBoundingClientRect();
	document.body.addEventListener( "mousedown", logMouseDown );
	document.body.addEventListener( "mousemove", logMouseMove );
	document.body.addEventListener( "mouseup", logMouseUp );

	$.onkey( [ "Control", "Alt", "g" ], "down", getLogs );

	function logMouseDown( e ) {
		logMove( "md", e );
		dragIndex = moves.length - 1;
		isMoving = true;
		isDragging = false;
	}

	function logMouseMove( e ) {
		var xy;
		xy = updateXY( e );
		if( isMoving ) {
			if( last.x !== xy.x || last.y !== xy.y ) {
				logMove( "mm", e );
			}
			isDragging = true;
		}
		last = {
			"x": xy.x,
			"y": xy.y
		};
	}

	function logMouseUp( e ) {
		var element = document.elementFromPoint( e.pageX, e.pageY );
		if( isDragging && element.id === "grid" ) {
			logMove( "mu", e );
		} else {
			moves.splice( dragIndex, moves.length - dragIndex );
			logMove( "click", e );
		}
		isMoving = false;
		isDragging = false;
	}

	function updateXY( e ) {
		x = Math.round( ( e.pageX - rect.left ) / rect.width * 1000 ) / 1000;
		y = Math.round( ( e.pageY - rect.top ) / rect.width * 1000 ) / 1000;
		return {
			"x": x,
			"y": y
		};
	}

	function logMove( move, e ) {
		var xy, element, value;
		element = document.elementFromPoint( e.pageX, e.pageY );
		value = "";
		if( element.nodeName === "INPUT" ) {
			value = element.value;
			move = "update";
		}

		if( element.id === "grid" ) {
			xy = updateXY( e );
			x = xy.x;
			y = xy.y;
		} else {
			x = 0;
			y = 0;
			move = "click";
		}

		moves.push( [ element.id, move, x, y, e.buttons, value ] );
	}

	function getLogs() {
		$.setActionKey( "c", false );
		$.setActionKey( "Control", false );

		var div = document.createElement( "div" );
		var textarea = document.createElement( "textarea" );
		var ok = document.createElement( "input" );
		ok.type = "button";
		ok.onclick = function () {
			div.parentElement.removeChild( div );
		};
		ok.value = "OK";
		div.style.position = "absolute";
		div.style.left = 0;
		div.style.top = 0;
		div.style.width = "calc(100% - 10px)";
		div.style.height = "calc(100% - 10px)";

		textarea.style.width = "calc(100% - 10px)";
		textarea.style.height = "calc(100% - 100px)";
		var strMove = "[\n";
		for( i = 0; i < moves.length; i++ ) {
			strMove += "\t[ ";
			var strLine = "";
			for( j = 0; j < moves[ i ].length; j++ ) {
				if( typeof moves[ i ][ j ] === "string" ) {
					strLine += "\"" + moves[ i ][ j ] + "\", ";
				} else {
					strLine += moves[ i ][ j ] + ", ";
				}
			}
			strLine = strLine.substr( 0, strLine.length - 2 );
			strMove += strLine + " ],\n";
		}
		strMove = strMove.substr( 0, strMove.length - 2 );
		strMove += "\n]";
		textarea.innerHTML = strMove;

		div.appendChild( textarea );
		div.appendChild( ok );
		document.body.appendChild( div );

		textarea.focus();
		textarea.selectionStart = 0;
		textarea.selectionEnd = textarea.innerHTML.length;
	}

}, 1500 );
